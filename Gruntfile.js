module.exports = function( grunt ) {
    grunt.initConfig( {
        qunit: {

            all: [
                    'lists/static/tests/tests.html'
            ]
        },
        qunit_junit: {
            options: {
                dest: './tmp/QUnit'
                // Task specific options go here.
            }
        },
    } );
    grunt.loadNpmTasks( "grunt-contrib-qunit" );
    grunt.loadNpmTasks('grunt-qunit-junit');
    grunt.registerTask('test', [ 'qunit_junit', 'qunit']);
};
